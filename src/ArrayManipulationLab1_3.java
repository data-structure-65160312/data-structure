import java.util.Scanner;

public class ArrayManipulationLab1_3 {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("input size of array nums1 :");
        int sizeA = sc.nextInt();
        System.out.println("input each element :");
        int[] nums1 = getNums(sc, sizeA);
        System.out.println("input size of array nums2 :");
        int sizeB = sc.nextInt();
        System.out.println("input each element :");
        int[] nums2 = getNums(sc, sizeB);

        int m = 0, n = 0;
        m = getRealSize(nums1);
        n = getRealSize(nums2);
        System.out.println(m + " " + n + " ");
        merge(nums1, m, nums2, n);
        printNumber(nums1);

    }

    private static int[] getNums(Scanner sc, int size) {
        int[] nums = new int[size];
        for (int i = 0; i < size; i++) {
            nums[i] = sc.nextInt();
        }
        return nums;
    }

    private static void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1;
        int j = n - 1;
        int k = m + n - 1;

        while (i >= 0 && j >= 0) {
            if (nums1[i] > nums2[j]) {
                nums1[k] = nums1[i];
                i--;
            } else {
                nums1[k] = nums2[j];
                j--;
            }
            k--;
        }

        while (j >= 0) {
            nums1[k] = nums2[j];
            j--;
            k--;
        }
    }

    private static int getRealSize(int[] nums) {
        int size = 0;
        for (int num : nums) {
            if (num != 0) {
                size++;
            }
        }
        return size;
    }

    private static void printNumber(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i]+" ");
        }
    }
}
