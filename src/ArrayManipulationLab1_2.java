import java.util.Scanner;

public class ArrayManipulationLab1_2 {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int sizeNum1 = sc.nextInt();
        int[] arr = new int[sizeNum1];
        for (int i = 0; i < sizeNum1; i++) {
            arr[i] = sc.nextInt();
        }
       

        duplicateZeros(arr);
        printNumber(arr);

      
    }

    private static void duplicateZeros(int[] arr) {
        int length = arr.length;
        int zerosCount = 0;

        for (int num : arr) {
            if (num == 0) {
                zerosCount++;
            }
        }

        for (int i = length - 1; i >= 0; i--) {
            if (arr[i] == 0) {
                if (i + zerosCount < length) {
                    arr[i + zerosCount] = 0;
                }
                zerosCount--;
            }
            if (i + zerosCount < length) {
                arr[i + zerosCount] = arr[i];
            }
        }
    }

    private static void printNumber(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i]+" ");
        }
    }
}
