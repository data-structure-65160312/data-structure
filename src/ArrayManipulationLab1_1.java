public class ArrayManipulationLab1_1 {
    static final int[] numbers = { 5, 8, 3, 2, 7 };
    static final String[] names = { "Alice", "Bob", "Charlie", "David" };
    static final double[] values = new double[4];
    static final int[] arr = { 1, 0, 2, 3, 0, 4, 5, 0 };

    public static void main(String[] args) throws Exception {

        printNumber(numbers);

        printArray(names);

        // initial each value
        values[0] = 2.1342;
        values[1] = 1.1213;
        values[2] = 5.2452;
        values[3] = 3.3421;

        getSumNumber();

        getMax();

        String[] reversedNames = new String[names.length];
        getReverseArray(reversedNames);

    }


    private static void getReverseArray(String[] reversedNames) {
        for (int start = 0, end = names.length - 1; start < names.length; start++, end--) {
            reversedNames[start] = names[end];
        }
        printArray(reversedNames);
    }

    private static void getMax() {
        double max = 0;
        for (double value : values) {
            if (max < value) {
                max = value;
            }
        }
        System.out.println("maximum of values is : " + max);
    }

    private static void getSumNumber() {
        int sum = 0;
        for (int n : numbers) {
            sum += n;
        }
        System.out.println("sum of all array :"+sum);
    }

    private static void printArray(String[] names) {
        for (String name : names) {
            System.out.print(name+" ");
        }
        System.out.println();
    }

    private static void printNumber(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }
}
